from django.db import models
from django.utils import timezone
from service.models import Service
from account.models import Account

class Menu(models.Model):
    title         = models.CharField(max_length=255, blank=True, null=True, default=None)
    menu_created  = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True, default=None)
    link          = models.TextField(blank=True, null=True, default=None)
    orderby       = models.IntegerField(blank=True, null=True, default=0)
    status        = models.BooleanField(default=False)

    user_created  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_menu")
    user_updated  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_menu")
    created_at	  = models.DateTimeField(default=timezone.now)
    updated_at	  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title