from django.urls import path
from menu.views import MenuView, MenuDetail

urlpatterns = [
    path("api/menu/", MenuView.as_view()),
    path("api/menu/<int:pk>/", MenuDetail.as_view()),
]