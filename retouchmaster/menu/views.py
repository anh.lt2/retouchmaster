from rest_framework.views import APIView, Response
from menu.models import Menu
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from menu.serializers import MenuViewSerializer

class MenuView(APIView):
    permission_classes = [IsAuthenticated]
     
    @swagger_auto_schema(
        tags=["Menu"],
        responses=status_response,
        operation_id='Get list Menu',
    )
    def get(self, request):
        menu = Menu.objects.all()
        serializer = MenuViewSerializer(menu, many=True)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Menu"],
        responses=status_response,
        operation_id='Create Menu',
        operation_description='Create Menu',
        request_body=swg.create_menu
    )
    @transaction.atomic
    def post(self, request):
        serializer = MenuViewSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class MenuDetail(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        tags=["Menu"],
        responses=status_response,
        operation_id='Get detail Menu',
        operation_description='Get detail Menu',
    )
    def get(self, request, pk):
        try:
            menu = Menu.objects.get(pk=pk)
        except Menu.DoesNotExist:
            return Response(convert_response("Menu not found", 400))
        serializer = MenuViewSerializer(menu)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Menu"],
        responses=status_response,
        operation_id='Update Menu',
        operation_description='Update Menu',
        request_body=swg.create_menu
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            menu = Menu.objects.get(pk=pk)
        except Menu.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = MenuViewSerializer(menu, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Menu"],
        responses=status_response,
        operation_id='Delete Menu',
        operation_description='Delete Menu',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try: 
            menu = Menu.objects.get(pk=pk)
        except Menu.DoesNotExist:
            return Response(convert_response("Menu not found", 400))
        menu.delete()
        return Response(convert_response("Success", 200))