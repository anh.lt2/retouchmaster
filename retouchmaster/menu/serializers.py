from rest_framework import serializers
from general.general import *
from menu.models import Menu

class MenuViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Menu
        fields = ["id", "titlte", "menu_created", "link", "orderby", "status", "user_created", "user_updated", "created_at", "updated_at"] 