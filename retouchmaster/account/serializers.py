from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from general.general import *
from account.models import Account


class AccountViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = ["id", "username", "phone", "email", "first_name", "last_name", "is_active", "birthday", "address", "avatar", "settings", "avatar", "user_created", "created_at"]