from django.urls import path
from account.views.account_views import AccountLogin, ChangePassword, DashboardView
from account.views.account_create import AccountDetail

urlpatterns = [
    path("api/login/", AccountLogin.as_view()),
    path("api/changepassword/", ChangePassword.as_view()),
    path("api/account/<int:pk>/", AccountDetail.as_view()),
    path("api/dashboard/", DashboardView.as_view())
]