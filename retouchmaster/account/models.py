from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from account.custommanager import CustomUserManager

class Account(AbstractUser):
    
    username        = models.CharField(max_length=20, unique=True)
    phone           = models.CharField(max_length=20, blank=True, null=True, default=None)
    email           = models.EmailField(max_length=50, blank=True, null=True, default=None)
    first_name      = models.CharField(max_length=255, default=None, blank=True, null=True)
    last_name       = models.CharField(max_length=255, null=True, blank=True, default=None)
    birthday        = models.DateField(null=True, blank=True, default=None)
    address         = models.CharField(max_length=255, default=None, blank=True, null=True)
    avatar          = models.ImageField(upload_to='account/avatar/', null=True, blank=True)
    settings        = models.JSONField(default=dict, blank=True)

    user_created    = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_account")
    created_at	    = models.DateTimeField(default=timezone.now)
    updated_at	    = models.DateTimeField(default=timezone.now)
   
    USERNAME_FIELD  = 'username'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.username or ''