from rest_framework.views import APIView, Response
from account.models import Account
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from django.contrib.auth import login
from django.contrib.auth.hashers import check_password
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from account.serializers import AccountViewSerializer
from trial.models import Trial
from contact.models import Contact
from quote.models import Quote


class AccountLogin(APIView):
    @swagger_auto_schema(
        tags=["Account"],
        responses=status_response,
        operation_id='Login',
        operation_description='Login',
        request_body = swg.api_login
    )
    def post(self, request):
        username = request.data.get("username", None)
        password = request.data.get("password", None)

        if not username or not password:
            return Response(convert_response("Required username, password", 400))
        try:
            account = Account.objects.get(username=username)
        except:
            return Response(convert_response("Account not found", 400))
        if not account.check_password(password):
            return Response(convert_response("Invalid password", 400))   
        if account.is_active == False:
            return Response(convert_response("Account not activate", 400))
        token, created = Token.objects.get_or_create(user=account)
        serializer = AccountViewSerializer(account)
        return Response(convert_response("Success", 200, {"token": token.key, "user": serializer.data}))
    
class ChangePassword(APIView):
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        tags=["Account"],
        responses=status_response,
        operation_id='Change Password',
        operation_description="Change Password",
        request_body = swg.api_change_password
    )
    def put(self, request):
        old_password = request.data.get("old_password", None)
        new_password = request.data.get("new_password", None)
        confirm_password = request.data.get("confirm_password", None)

        user = request.user
        if not old_password or not new_password or not confirm_password:
            return Response(convert_response("Required old_password, new_password, confirm_password", 400))
        if new_password == old_password:
            return Response(convert_response("New_password and Old_password should not be the same", 400))
        if new_password != confirm_password:
                return Response(convert_response("Required new_password and confirm_password must the same", 400))        
        account = Account.objects.get(pk=user.id)
        if not check_password(old_password, account.password):
            return Response(convert_response("Old_password is invalid", 400))
        account.set_password(new_password)
        account.save()
        return Response(convert_response("Success", 200))
    

class DashboardView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]


    @swagger_auto_schema(
        tags=["Dashboard"],
        responses=status_response,
        operation_id='Get Dashboard',
        operation_description="Get Dashboard",
    )
    def get(self, request):
        response = {
            "quote": Quote.objects.all().count(),
            "trial" : Trial.objects.all().count(),
            "contact": Contact.objects.all().count()
        }
        return Response(convert_response("Success", 200, response))