from rest_framework.views import APIView, Response
from account.models import Account
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from account.serializers import AccountViewSerializer


class AccountDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Account"],
        responses=status_response,
        operation_id='Detail Account',
        operation_description='Detail Account',
    )
    def get(self, request, pk):
        try:
            account = Account.objects.get(pk=pk)
        except Account.DoesNotExist:
            return Response(convert_response("Account not found", 400))
        serializer = AccountViewSerializer(account)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Account"],
        responses=status_response,
        operation_id='Update Account',
        operation_description='Update Account',
        request_body = swg.update_account
    )
    def put(self, request, pk):
        try:
            account = Account.objects.get(pk=pk)
        except Account.DoesNotExist:
            return Response(convert_response("Account not found", 400))
        serializer = AccountViewSerializer(account, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400, serializer.errors))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))