from django.db import models
from django.utils import timezone

class Document(models.Model):
    image       = models.FileField(upload_to='document/image/')
    alt         = models.CharField(max_length=255, null=True, blank=True, default=None)
    created_at  = models.DateTimeField(default=timezone.now)
    updated_at  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.alt