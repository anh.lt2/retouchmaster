from django.urls import path
from document.views import PostDocument

urlpatterns = [
    path("api/postdocument/", PostDocument.as_view()),
]