from rest_framework.views import APIView, Response
from drf_yasg.utils import swagger_auto_schema
from django.db import transaction
import generic.swagger_example as swg
from general.general import *
from document.serializers import CreateMultipleDocumentSerializer

class PostDocument(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Document"],
        responses=status_response,
        operation_id='Create Document Media',
        operation_description='Create Document Media',
        request_body=swg.post_document
    )
    @transaction.atomic
    def post(self, request):
        image_file = request.data.get("image_file", None)
        serializer_media = CreateMultipleDocumentSerializer(data=request.data)
        if not serializer_media.is_valid():
            return Response(convert_response("Error", 400, serializer_media.errors))
        media_instance = serializer_media.save()
        media_data = [{"id": media.id, "image": media.image.url, "alt": media.alt} for media in media_instance]
        return Response(convert_response("Success", 200, media_data))