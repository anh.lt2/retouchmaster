from rest_framework import serializers
from general.general import *
from document.models import Document

class CreateMultipleDocumentSerializer(serializers.Serializer):
    image_file = serializers.ListField(child=serializers.FileField(), write_only=True, required=True)

    def create(self, validate_data):
        media_list = []
        for item in validate_data["image_file"]:
            media = Document.objects.create(image=item, alt=item.name)
            media_list.append(media)
        return media_list