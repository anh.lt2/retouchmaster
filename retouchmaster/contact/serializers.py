from rest_framework import serializers
from general.general import *
from contact.models import Contact

class ContactViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contact
        fields = ["id", "customer_name", "email", "service", "your_project", "link_image", "user_created", "user_updated", "created_at", "updated_at"]