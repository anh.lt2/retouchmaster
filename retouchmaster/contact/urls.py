from django.urls import path
from contact.views import ContactView, ContactDetail

urlpatterns = [
    path("api/contact/", ContactView.as_view()),
    path("api/contact/<int:pk>/", ContactDetail.as_view()),
]