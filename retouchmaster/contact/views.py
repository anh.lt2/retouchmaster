from rest_framework.views import APIView, Response
from contact.models import Contact
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from django.db.models import Q
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from contact.serializers import ContactViewSerializer

class ContactView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Contact"],
        responses=status_response,
        operation_id='Get list Contact',
        operation_description='Get list Contact'
    )
    def get(self, request):
        QUERY_ACCEPT = ["customer_name", "service", "require_auth", "search"]
        SEARCH_FIELDS = ["customer_name", "email", "your_project", "link_image"]

        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        contact = Contact.objects.filter(**query_params).filter(search_query)
        serializer = ContactViewSerializer(contact, many=True)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Contact"],
        responses=status_response,
        operation_id='Create Contact',
        operation_description='Create Contact',
        request_body=swg.post_contact
    )
    @transaction.atomic
    def post(self, request):
        serializer = ContactViewSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class ContactDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Contact"],
        responses=status_response,
        operation_id='Get detail Contact',
        operation_description='Get detail Contact',
    )
    def get(self, request, pk):
        try:
            contact = Contact.objects.get(pk=pk)
        except Contact.DoesNotExist:
            return Response(convert_response("Contact not found", 400))
        serializer = ContactViewSerializer(contact)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Contact"],
        responses=status_response,
        operation_id='Update Contact',
        operation_description='Update Contact',
        request_body=swg.post_contact
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            contact = Contact.objects.get(pk=pk)
        except Contact.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = ContactViewSerializer(contact, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Contact"],
        responses=status_response,
        operation_id='Delete Contact',
        operation_description='Delete Contact',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            contact = Contact.objects.get(pk=pk)
        except Contact.DoesNotExist:
            return Response(convert_response("Contact not found", 400))
        contact.delete()
        return Response(convert_response("Success", 200))