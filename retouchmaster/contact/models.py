from django.db import models
from django.utils import timezone
from service.models import Service
from account.models import Account

class Contact(models.Model):
    customer_name = models.CharField(max_length=255, blank=True, null=True, default=None)
    email         = models.CharField(max_length=255, blank=True, null=True, default=None)
    service       = models.ForeignKey(Service, on_delete=models.CASCADE, null=True, blank=True, default=None)
    your_project  = models.TextField(null=True, blank=True, default=None)
    link_image    = models.TextField(null=True, blank=True, default=None)

    user_created  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_contact")
    user_updated  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_contact")
    created_at	  = models.DateTimeField(default=timezone.now)
    updated_at	  = models.DateTimeField(default=timezone.now)