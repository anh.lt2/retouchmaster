from django.urls import path
from service.views import ServiceView, ServiceDetail, SectionDetail, SectionView, UpdateListSection, DeleteListSection, DeleteService, UpdateService, SelectService, ServiceDetailWithID, ServiceViewAdmin

urlpatterns = [
    path("api/service/", ServiceView.as_view()),
    path("api/service/<str:slug>/", ServiceDetail.as_view()),
    path("api/getdetailservice/<int:pk>/", ServiceDetailWithID.as_view()),
    path("api/updateservice/<int:pk>/", UpdateService.as_view()),
    path("api/deleteservice/<int:pk>/", DeleteService.as_view()),
    path("api/section/", SectionView.as_view()),
    path("api/section/<int:pk>/", SectionDetail.as_view()),
    path("api/updatelistsection/", UpdateListSection.as_view()),
    path("api/deletelistsection/", DeleteListSection.as_view()),
    path("api/serviceselect/", SelectService.as_view()), 
    path("api/serviceadmin/", ServiceViewAdmin.as_view()),   
]