from django.contrib import admin
from service.models import Service, MediaService, Section

# Register your models here.
admin.site.register(Service)
admin.site.register(MediaService)
admin.site.register(Section)