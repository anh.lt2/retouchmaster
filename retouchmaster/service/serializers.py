from rest_framework import serializers
from general.general import *
from service.models import Service, MediaService, Section

class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaService
        fields = ["id", "image", "alt", "created_at", "updated_at"]

class CreateMultipleMediaSerializer(serializers.Serializer):
    media_service = serializers.ListField(child=serializers.FileField())

    def create(self, validate_data):
        try:
            media = MediaService._default_manager.bulk_create(
                MediaService(image=image, alt=image.name) for image in validate_data["media_service"]
            )
            return media
        except:
            raise serializers.ValidationError({"Error" : "Invalids params media_service"})

class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = ["id", "image_before", "image_after", "content", "service", "created_at", "updated_at"]

class ServiceSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service 
        fields = ["id", "title", "status", "url", "created_at", "updated_at"]

class ServiceCreateSerializer(serializers.ModelSerializer):
    media_data = MediaSerializer(source="media", read_only=True, many=True)
    section_data = SectionSerializer(source="service_section", read_only=True, many=True)
    service_created_data = ServiceSelectSerializer(source="service_created", read_only=True)
    class Meta:
        model = Service
        fields = ["id", "title", "content", "status", "url", "media", "media_data", "service_created", "service_created_data", "section_data", "user_created", "user_updated", "created_at", "updated_at"]

    def create(self, validated_data):
        user = self.context["request"].user
        validated_data["user_created"] = user
        validated_data["user_updated"] = user
        return super().create(validated_data)
    
class ServiceViewSerializer(serializers.ModelSerializer):
    section_data = SectionSerializer(source="service_section", read_only=True, many=True)
    class Meta:
        model = Service
        fields = ["id", "title", "content", "status", "url", "media", "section_data", "created_at", "updated_at"]
    
class ServiceViewAdminSerializer(serializers.ModelSerializer):
    media_data = MediaSerializer(source="media", read_only=True, many=True)
    service_created_data = ServiceSelectSerializer(source="service_created", read_only=True)
    class Meta:
        model = Service
        fields = ["id", "title", "status", "url", "media", "media_data", "service_created", "service_created_data"]
