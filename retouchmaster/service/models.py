from django.db import models
from django.utils import timezone
from account.models import Account

class MediaService(models.Model):
    image       = models.FileField(upload_to='service/image/')
    alt         = models.CharField(max_length=255, null=True, blank=True, default=None)
    created_at  = models.DateTimeField(default=timezone.now)
    updated_at  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.alt

class Service(models.Model):
    title           = models.CharField(max_length=255, blank=True, null=True, default=None)
    content         = models.TextField(blank=True, null=True, default=None)
    status          = models.BooleanField(blank=True, null=True, default=False)
    media           = models.ManyToManyField(MediaService, blank=True)
    url             = models.CharField(max_length=255, blank=True, null=True, default=None, unique=True)
    service_created = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="service_created_service")

    user_created    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_service")
    user_updated    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_service")
    created_at	    = models.DateTimeField(default=timezone.now)
    updated_at	    = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

class Section(models.Model):
    image_before = models.FileField(upload_to='section/image/', null=True, blank=True)
    image_after  = models.FileField(upload_to='section/image/', null=True, blank=True)
    content      = models.TextField(blank=True, null=True, default=None)
    service      = models.ForeignKey(Service, on_delete=models.CASCADE, null=True, blank=True, default=None, related_name="service_section")

    created_at	 = models.DateTimeField(default=timezone.now)
    updated_at	 = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.service.title


