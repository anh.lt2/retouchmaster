from rest_framework.views import APIView, Response
from service.models import Service, Section, MediaService
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from service.serializers import ServiceCreateSerializer, CreateMultipleMediaSerializer, SectionSerializer, ServiceSelectSerializer, ServiceViewAdminSerializer, ServiceViewSerializer
import json

class ServiceView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Get list Service',
        operation_description='Get list Service filter =["customer_name", "email", "note", "service__title", "require_auth", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["customer_name", "email", "note", "service__title", "require_auth", "search"]
        SEARCH_FIELDS = ["customer_name", "email", "note", "service__title"]
        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        service = Service.objects.filter(**query_params).filter(search_query)
        count = service.count() 
        service = Paginate(service, request.GET)
        serializer = ServiceViewSerializer(service, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))
    
    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Create Service',
        operation_description='Create Service',
        request_body=swg.create_service
    )
    @transaction.atomic
    def post(self, request):
        media_service = request.data.get("media_service", None)
        image_before_section = request.FILES.getlist("image_before", None)
        image_after_section = request.FILES.getlist("image_after", None)
        data = request.data.get("data", None)
        data = json.loads(data)
        service = data.get("service", None)
        section = data.get("section", None)
        list_image_before = data.get("list_image_before", None)
        list_image_after = data.get("list_image_after", None)
        
        # Create service
        serializer = ServiceCreateSerializer(data=service, context={"request": request})
        if not serializer.is_valid():
            return Response(convert_response("Invalid params service", 400, serializer.errors))
        service_instance = serializer.save()
        if media_service:
            serializer_media = CreateMultipleMediaSerializer(data=request.data)
            if not serializer_media.is_valid():
                service_instance.delete()
                return Response(convert_response("Error", 400, serializer_media.errors))
            media_instance = serializer_media.save()
            service_instance.media.set(media_instance)
        
        image_before_list = []
        image_after_list = []
        if section:
            for index, image_item_before in enumerate(list_image_before):
                if image_item_before:
                    image_before_list.append(index)
            for index, image_item_after in enumerate(list_image_after):
                if image_item_after:
                    image_after_list.append(index)

            for i in range(len(image_before_list)):
                section[image_before_list[i]]["image_before"] = image_before_section[i]
            for j in range(len(image_after_list)):
                section[image_after_list[j]]["image_after"] = image_after_section[j]

            section_serializer = SectionSerializer(data=section, many=True)
            if not section_serializer.is_valid():
                service_instance.delete()
                media_instance.delete()
                return Response(convert_response("Invalid params section", 400, section_serializer.errors))
            variant_instance = section_serializer.save(service=service_instance)

        return Response(convert_response("Success", 200, serializer.data))
    
class ServiceDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Get Detail Service',
        operation_description='Get Detail Service'
    )
    def get(self, request, slug):
        try:
            service = Service.objects.get(url=slug)
        except Service.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = ServiceViewSerializer(service)
        return Response(convert_response("Success", 200, serializer.data))

class UpdateService(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Update Service',
        operation_description='Update Service',
        request_body=swg.update_service
    )
    @transaction.atomic
    def put(self, request, pk):
        media_service = request.data.get("media_service", None)
        image_before_section = request.FILES.getlist("image_before", None)
        image_after_section = request.FILES.getlist("image_after", None)
        data = request.data.get("data", None)
        data = json.loads(data)
        service = data.get("service", None)
        section = data.get("section", None) 
        list_image_before = data.get("list_image_before", None)
        list_image_after = data.get("list_image_after", None)

        try:
            service_obj = Service.objects.get(pk=pk)
        except:
            return Response(convert_response("Service not found"))
         
        if service:
            serializer_service = ServiceCreateSerializer(service_obj, data=service, partial=True)
            if not serializer_service.is_valid():
                return Response(convert_response("Invalid params service", 200, serializer_service.errors))
            service_instance = serializer_service.save()
        else:
            serializer_service =ServiceCreateSerializer(service_obj)
        
        if media_service:
            serializer_media = CreateMultipleMediaSerializer(data=request.data)
            if not serializer_media.is_valid():
                return Response(convert_response("Error", 400, serializer_media.errors))
            media_instance = serializer_media.save()
            media_ids = service_instance.media.values_list("id", flat=True)
            MediaService.objects.filter(id__in=media_ids).delete()
            service_instance.media.add(*media_instance)

        if section:
            new_create = []
            if "delete" in section:
                Section.objects.filter(id__in=section["delete"]).delete()

            if "update_str" in section:
                ids = [item["id"] for item in section["update_str"]]
                for item in section["update_str"]:
                    section_instance  = Section.objects.get(pk=item["id"])
                    serializer = SectionSerializer(section_instance, data=item, partial=True)
                    if not serializer.is_valid():
                        return Response(convert_response("Error", 400, serializer_media.errors))
                    serializer.save()

            if "update_image" in section:
                ids = [item["id"] for item in section["update_image"]]
                Section.objects.filter(id__in=ids).delete()
                new_create.extend(section["update_image"])

            if "create" in section:
                new_create.extend(section["create"])
                
            if new_create:
                image_before_list = []
                image_after_list = []
                for index, image_item_before in enumerate(list_image_before):
                    if image_item_before:
                        image_before_list.append(index)
                for index, image_item_after in enumerate(list_image_after):
                    if image_item_after:
                        image_after_list.append(index)  

                for i in range(len(image_before_list)):
                    new_create[image_before_list[i]]["image_before"] = image_before_section[i]
                for j in range(len(image_after_list)):
                    new_create[image_after_list[j]]["image_after"] = image_after_section[j]
                for item in new_create:
                    item["service"] = service_obj.id
                section_serializer = SectionSerializer(data=new_create, many=True)
                if not section_serializer.is_valid():
                    return Response(convert_response("Invalid params section", 400, section_serializer.errors))
                section_serializer.save() 
        return Response(convert_response("Success", 200, serializer_service.data))
    
class DeleteService(APIView): 
    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Delete Service',
        operation_description='Delete Service',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            service = Service.objects.get(pk=pk)
        except Service.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        service.delete()
        return Response(convert_response("Success", 200))

class SectionView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        tags=["Section"],
        responses=status_response,
        operation_id='Create Section',
        operation_description='Create Section',
        request_body=swg.create_section
    )
    @transaction.atomic
    def post(self, request):
        image_before_section = request.FILES.getlist("image_before", None)
        image_after_section = request.FILES.getlist("image_after", None)
        data = request.data.get("data", None)
        service = request.data.get("service", None)
        data = json.loads(data)
        section = data.get("section", None)
        list_image_before = data.get("list_image_before", None)
        list_image_after = data.get("list_image_after", None)

        image_before_list = []
        image_after_list = []
        for index, image_item_before in enumerate(list_image_before):
            if image_item_before:
                image_before_list.append(index)
        for index, image_item_after in enumerate(list_image_after):
            if image_item_after:
                image_after_list.append(index)
        for i in range(len(image_before_list)):
            section[image_before_list[i]]["image_before"] = image_before_section[i]
        for j in range(len(image_after_list)):
            section[image_after_list[j]]["image_after"] = image_after_section[j]
        for item in section:
            item["service"] = service
        section_serializer = SectionSerializer(data=section, many=True)
        if not section_serializer.is_valid():
            return Response(convert_response("Invalid params section", 400, section_serializer.errors))
        section_serializer.save()
        return Response(convert_response("Success", 200, section_serializer.data))
    
class SectionDetail(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        tags=["Section"],
        responses=status_response,
        operation_id='Get Detail Section',
        operation_description='Get Detail Section'
    )
    def get(self, request, pk):
        try:
            section = Section.objects.get(pk=pk)
        except Section.DoesNotExist:
            return Response(convert_response("Section not found", 400))
        serializer = SectionSerializer(section)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Section"],
        responses=status_response,
        operation_id='Update Section',
        operation_description='Update Section',
        request_body=swg.create_section
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            section = Section.objects.get(pk=pk)
        except Section.DoesNotExist:
            return Response(convert_response("Section not found", 400))
        serializer = SectionSerializer(section, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Section"],
        responses=status_response,
        operation_id='Delete Section',
        operation_description='Delete Section',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            section = Section.objects.get(pk=pk)
        except Section.DoesNotExist:
            return Response(convert_response("Section not found", 400))
        section.delete()
        return Response(convert_response("Success", 200))
    
class UpdateListSection(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        tags=["Section"],
        responses=status_response,
        operation_id='Update List Section',
        operation_description='Update List Section',
        request_body=swg.update_section
    )
    @transaction.atomic
    def post(self, request):
        section_ids = request.data.get("section_ids", [])
        image_before_section = request.FILES.getlist("image_before", None)
        image_after_section = request.FILES.getlist("image_after", None)
        data = request.data.get("data", None)
        service = request.data.get("service", None)
        data = json.loads(data)
        section = data.get("section", None)
        list_image_before = data.get("list_image_before", None)
        list_image_after = data.get("list_image_after", None)

        sections = Section.objects.filter(id__in=section_ids)
        sections.delete()
        image_before_list = []
        image_after_list = []
        for index, image_item_before in enumerate(list_image_before):
            if image_item_before:
                image_before_list.append(index)
        for index, image_item_after in enumerate(list_image_after):
            if image_item_after:
                image_after_list.append(index)
        for i in range(len(image_before_list)):
            section[image_before_list[i]]["image_before"] = image_before_section[i]
        for j in range(len(image_after_list)):
            section[image_after_list[j]]["image_after"] = image_after_section[j]
        for item in section:
            item["service"] = service
        section_serializer = SectionSerializer(data=section, many=True)
        if not section_serializer.is_valid():
            return Response(convert_response("Invalid params section", 400, section_serializer.errors))
        section_serializer.save()
        return Response(convert_response("Success", 200, section_serializer.data))
    
class DeleteListSection(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        tags=["Section"],
        responses=status_response,
        operation_id='Delete List Section',
        operation_description='Delete List Section',
        request_body=swg.delete_section
    )
    @transaction.atomic
    def post(self, request):
        section_ids = request.data.get("section_ids", [])
        sections = Section.objects.filter(id__in=section_ids)
        sections.delete()
        return Response(convert_response("Success", 200))

class SelectService(APIView):

    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Get Select Service',
        operation_description='Get Select Service filter=["title", "content", "description", "summary", "category", "status_new", "subscribers", "status", "is_highlight", "top_blog", "lastest_blog", "created_at__lte", "created_at__gte", "random", "require_auth", "search"]',
    )
    def get(self, request):
        QUERY_ACCEPT = ["title", "content", "description", "summary", "category", "status_new", "subscribers", "status", "is_highlight", "top_blog", "lastest_blog", "created_at__lte", "created_at__gte", "random", "require_auth", "search"]
        BOOL_ACCEPT = ["status", "status_new", "is_highlight", "top_blog", "lastest_blog"]
        query_params = request.query_params.dict().copy()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key in BOOL_ACCEPT:
                if query_params[key] == "true":
                    query_params[key] = True
                elif query_params[key] == "false":
                    query_params[key] = False
        service = Service.objects.filter(**query_params)
        serializer = ServiceSelectSerializer(service, many=True)
        return Response(convert_response("Success", 200, serializer.data))


class ServiceDetailWithID(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Get Detail With id Service',
        operation_description='Get Detail With id Service'
    )
    def get(self, request, pk):
        try:
            service = Service.objects.get(pk=pk)
        except Service.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = ServiceViewSerializer(service)
        return Response(convert_response("Success", 200, serializer.data))
    
class ServiceViewAdmin(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Service"],
        responses=status_response,
        operation_id='Get list Service Admin',
        operation_description='Get list Service Admin filter =["customer_name", "email", "note", "service__title", "require_auth", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["customer_name", "email", "note", "service__title", "require_auth", "search"]
        SEARCH_FIELDS = ["customer_name", "email", "note", "service__title"]
        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        service = Service.objects.filter(**query_params).filter(search_query)
        count = service.count() 
        service = Paginate(service, request.GET)
        serializer = ServiceViewAdminSerializer(service, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))