from drf_yasg import openapi

"""
    ACCOUNT
"""

#swagger login
api_login = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties= {
        "username" : openapi.Schema(type=openapi.TYPE_STRING, description="phone", default="admin"),
        "password" : openapi.Schema(type=openapi.TYPE_STRING, description="password", default="1"),
    }
)

#swagger change_password 
api_change_password = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "old_password" : openapi.Schema(type=openapi.TYPE_STRING, description="old_password", default="123545678"),
        "new_password" : openapi.Schema(type=openapi.TYPE_STRING, description="new_password", default="123545678"),
        "confirm_password" : openapi.Schema(type=openapi.TYPE_STRING, description="confirm_password", default="123545678"),
    }
)

update_account = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "username" : openapi.Schema(type=openapi.TYPE_STRING, description="username", default="admin"),
        "phone" : openapi.Schema(type=openapi.TYPE_STRING, description="phone", default="123545678"),
        "email" : openapi.Schema(type=openapi.TYPE_STRING, description="email", default="a@gmail.com"),
        "first_name" : openapi.Schema(type=openapi.TYPE_STRING, description="first_name", default="Nguyen Van"),
        "last_name" : openapi.Schema(type=openapi.TYPE_STRING, description="last_name", default="A"),
        "birthday" : openapi.Schema(type=openapi.TYPE_STRING, description="birthday", default="2023-08-10"),
        "address" : openapi.Schema(type=openapi.TYPE_STRING, description="address", default="a@gmail.com"),
        "avatar" : openapi.Schema(type=openapi.TYPE_FILE, description="avatar", default="Form data"),

    }
)

# Answer question

post_answerquestion =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "answer" : openapi.Schema(type=openapi.TYPE_STRING, description="answer", default="test"),
        "question" : openapi.Schema(type=openapi.TYPE_STRING, description="question", default="test"),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

# Contact

post_contact =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "customer_name" : openapi.Schema(type=openapi.TYPE_STRING, description="customer_name", default="Nguyen Van A"),
        "email" : openapi.Schema(type=openapi.TYPE_STRING, description="email", default="NguyenVanA@gmail.com"),
        "service" : openapi.Schema(type=openapi.TYPE_INTEGER, description="service", default=1),
        "your_project" : openapi.Schema(type=openapi.TYPE_STRING, description="your_project", default="test"),
        "link_image" : openapi.Schema(type=openapi.TYPE_FILE, description="link_image", default="test"),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

# Feedback

post_feedback =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "customer_name" : openapi.Schema(type=openapi.TYPE_STRING, description="customer_name", default="Nguyen Van A"),
        "position" : openapi.Schema(type=openapi.TYPE_STRING, description="email", default="NguyenVanA@gmail.com"),
        "rate" : openapi.Schema(type=openapi.TYPE_INTEGER, description="rate", default="test"),
        "avatar" : openapi.Schema(type=openapi.TYPE_FILE, description="avatar", default="Form data"),
        "evaluate" : openapi.Schema(type=openapi.TYPE_STRING, description="evaluate", default="test"),
        "status": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

# Trial

post_trial =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "media_trial" : openapi.Schema(type=openapi.TYPE_FILE, description="media_trial", default="Form data"),
        "customer_name" : openapi.Schema(type=openapi.TYPE_STRING, description="customer_name", default="Nguyen Van A"),
        "email" : openapi.Schema(type=openapi.TYPE_STRING, description="email", default="NguyenVanA@gmail.com"),
        "note" : openapi.Schema(type=openapi.TYPE_STRING, description="note", default="test"),
        "url" : openapi.Schema(type=openapi.TYPE_STRING, description="url", default="test"),
        "status": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
        "service" : openapi.Schema(type=openapi.TYPE_INTEGER, description="service", default=1),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

post_media =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "media_trial" : openapi.Schema(type=openapi.TYPE_FILE, description="media_trial", default="Form data"),
    }
)

post_document =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "image_file" : openapi.Schema(type=openapi.TYPE_FILE, description="image_file", default="Form data"),
    }
)
# Post

create_post =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "tag" : openapi.Schema(type=openapi.TYPE_STRING, description="tag", default="Nguyen Van A"),
        "title" : openapi.Schema(type=openapi.TYPE_STRING, description="title", default="Nguyen Van A"),
        "summary" : openapi.Schema(type=openapi.TYPE_STRING, description="summary", default="test"),
        "description" : openapi.Schema(type=openapi.TYPE_STRING, description="description", default="test"),
        "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="test"),
        "content_bonus" : openapi.Schema(type=openapi.TYPE_STRING, description="content_bonus", default="test"),
        "image": openapi.Schema(type=openapi.TYPE_FILE, description="image", default="Form data"),
        "media_post": openapi.Schema(type=openapi.TYPE_FILE, description="media_post", default="Form data"),
        "is_highlight": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="is_highlight", default=False),
        "status": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
        "status_new": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
        "top_blog": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="top_blog", default=False),
        "lastest_blog": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="lastest_blog", default=False),
        "subscribers" : openapi.Schema(type=openapi.TYPE_STRING, description="subscribers", default="Nguyen Van A"),
        "category" : openapi.Schema(type=openapi.TYPE_INTEGER, description="category", default=1),
    }
)

# Category

create_category=  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "name" : openapi.Schema(type=openapi.TYPE_STRING, description="customer_name", default="Nguyen Van A"),
        "pathurl" : openapi.Schema(type=openapi.TYPE_STRING, description="summary", default="test"),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

# Product

create_product =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "titlte" : openapi.Schema(type=openapi.TYPE_STRING, description="titlte", default="Nguyen Van A"),
        "image_before" : openapi.Schema(type=openapi.TYPE_FILE, description="image_before", default="Form data"),
        "image_after" : openapi.Schema(type=openapi.TYPE_FILE, description="image_after", default="Form data"),
        "image_example" : openapi.Schema(type=openapi.TYPE_FILE, description="image_example", default="Form data"),
        "video_url": openapi.Schema(type=openapi.TYPE_STRING, description="video_url", default=False),
        "content": openapi.Schema(type=openapi.TYPE_STRING, description="content", default="mo ta"),
        "sort" : openapi.Schema(type=openapi.TYPE_INTEGER, description="sort", default=1),
        "status_active" : openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status_active", default=False),
        "status_home" : openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status_home", default=False),
        "service" : openapi.Schema(type=openapi.TYPE_INTEGER, description="service", default=1),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

# Quote

create_quote =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "media_quote" : openapi.Schema(type=openapi.TYPE_FILE, description="media_quote", default="Form data"),
        "customer_name" : openapi.Schema(type=openapi.TYPE_STRING, description="customer_name", default="Nguyen Van A"),
        "email" : openapi.Schema(type=openapi.TYPE_STRING, description="email", default="test@gmail.com"),
        "note" : openapi.Schema(type=openapi.TYPE_STRING, description="note", default="note"),
        "url" : openapi.Schema(type=openapi.TYPE_STRING, description="url", default="test"),
        "status": openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
        "service" : openapi.Schema(type=openapi.TYPE_INTEGER, description="service", default=1),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

# Service

create_service =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "image_before": openapi.Schema(type=openapi.TYPE_FILE, description="list image_before", default="Image form data"),
        "image_after" : openapi.Schema(type=openapi.TYPE_STRING, description="list image_after", default="Image form data"),
        "media_service" : openapi.Schema(type=openapi.TYPE_STRING, description="list media_service", default="Image form data"),
        "data": openapi.Schema(type=openapi.TYPE_OBJECT, description="data",
            properties={
                "service": openapi.Schema(type=openapi.TYPE_OBJECT, description="service",
                    properties= {
                        "title" : openapi.Schema(type=openapi.TYPE_STRING, description="title", default="clipping path"),
                        "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="test"),
                        "status" : openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
                        "url" : openapi.Schema(type=openapi.TYPE_STRING, description="url", default="clipping-path"),
                        "service_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="service_created", default=1)}),
                "section": openapi.Schema(type=openapi.TYPE_ARRAY,
                    items=openapi.Schema(type=openapi.TYPE_OBJECT, description="service",
                        properties={
                            "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="test"),
                        }           
                    )),
                'list_image_before' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_before', default=[True, False]),
                'list_image_after' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_after', default=[True, False]),

        })
    }
)

update_service =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "section_ids": openapi.Schema(type=openapi.TYPE_ARRAY, default=[1, 2],
                    items=openapi.Schema(type=openapi.TYPE_OBJECT, description="service")),
        "image_before": openapi.Schema(type=openapi.TYPE_FILE, description="list image_before", default="Image form data"),
        "image_after" : openapi.Schema(type=openapi.TYPE_STRING, description="list image_after", default="Image form data"),
        "media_service" : openapi.Schema(type=openapi.TYPE_STRING, description="list media_service", default="Image form data"),
        "data": openapi.Schema(type=openapi.TYPE_OBJECT, description="data",
            properties={
                "service": openapi.Schema(type=openapi.TYPE_OBJECT, description="service",
                    properties= {
                        "title" : openapi.Schema(type=openapi.TYPE_STRING, description="title", default="test"),
                        "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="test"),
                        "status" : openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
                        "service_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="service_created", default=1)}),
                "section": openapi.Schema(type=openapi.TYPE_ARRAY,
                    items=openapi.Schema(type=openapi.TYPE_OBJECT, description="service",
                        properties={
                            "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="test"),
                        }           
                    )),
                'list_image_before' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_before', default=[True, False]),
                'list_image_after' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_after', default=[True, False]),

        })
    }
)

create_section = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "image_before": openapi.Schema(type=openapi.TYPE_FILE, description="list image_before", default="Image form data"),
        "image_after" : openapi.Schema(type=openapi.TYPE_STRING, description="list image_after", default="Image form data"),
        "service": openapi.Schema(type=openapi.TYPE_INTEGER, description="service", default=1),
        "data": openapi.Schema(type=openapi.TYPE_OBJECT, description="data",
            properties={
                "section": openapi.Schema(type=openapi.TYPE_ARRAY,
                    items=openapi.Schema(type=openapi.TYPE_OBJECT, description="service",
                        properties={
                            "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="test"),
                        }           
                    )),
                'list_image_before' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_before', default=[True, False]),
                'list_image_after' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_after', default=[True, False]),
        })
    }
)


update_section = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "section_ids": openapi.Schema(type=openapi.TYPE_ARRAY, default=[1, 2],
                    items=openapi.Schema(type=openapi.TYPE_OBJECT, description="service")),
        "image_before": openapi.Schema(type=openapi.TYPE_FILE, description="list image_before", default="Image form data"),
        "image_after" : openapi.Schema(type=openapi.TYPE_STRING, description="list image_after", default="Image form data"),
        "service": openapi.Schema(type=openapi.TYPE_INTEGER, description="service", default=1),
        "data": openapi.Schema(type=openapi.TYPE_OBJECT, description="data",
            properties={
                "section": openapi.Schema(type=openapi.TYPE_ARRAY,
                    items=openapi.Schema(type=openapi.TYPE_OBJECT, description="service",
                        properties={
                            "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="test"),
                        }           
                    )),
                'list_image_before' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_before', default=[True, False]),
                'list_image_after' : openapi.Schema(type=openapi.TYPE_STRING, description='list_image_after', default=[True, False]),
        })
    }
)


delete_section = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "section_ids": openapi.Schema(type=openapi.TYPE_ARRAY, default=[1, 2],
                    items=openapi.Schema(type=openapi.TYPE_OBJECT, description="service"))})

# Menu

create_menu =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "title" : openapi.Schema(type=openapi.TYPE_STRING, description="title", default="test"),
        "link" : openapi.Schema(type=openapi.TYPE_STRING, description="link", default="pathurl"),
        "orderby" : openapi.Schema(type=openapi.TYPE_INTEGER, description="orderby", default="note"),
        "status" : openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
        "menu_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="menu_created", default=1),
        "user_created" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_created", default=1),
        "user_updated" : openapi.Schema(type=openapi.TYPE_INTEGER, description="user_updated", default=1),
    }
)

# PROCESSES
post_processes =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "title" : openapi.Schema(type=openapi.TYPE_STRING, description="title", default="test"),
        "content" : openapi.Schema(type=openapi.TYPE_STRING, description="link", default="pathurl"),
        "image" : openapi.Schema(type=openapi.TYPE_INTEGER, description="orderby", default="note"),
        "status" : openapi.Schema(type=openapi.TYPE_BOOLEAN, description="status", default=False),
    }
)

post_pricing =  openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "price" : openapi.Schema(type=openapi.TYPE_INTEGER, description="price", default=200),
        "content" : openapi.Schema(type=openapi.TYPE_STRING, description="content", default="content"),
        "service" : openapi.Schema(type=openapi.TYPE_INTEGER, description="service", default=1),
    }
)