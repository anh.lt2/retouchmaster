from django.contrib import admin
from answerquestion.models import AnswerQuestion

# Register your models here.
admin.site.register(AnswerQuestion)