from rest_framework import serializers
from general.general import *
from answerquestion.models import AnswerQuestion

class AnswerQuesionViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = AnswerQuestion
        fields = ["id", "answer", "question", "user_created", "user_updated", "created_at", "updated_at"]