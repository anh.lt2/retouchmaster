from rest_framework.views import APIView, Response
from answerquestion.models import AnswerQuestion
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from django.contrib.auth.hashers import check_password
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from answerquestion.serializers import AnswerQuesionViewSerializer

class AnswerQuestionView(APIView):

    @swagger_auto_schema(
        tags=["Answer Question"],
        responses=status_response,
        operation_id='Get list Answer Question',
        operation_description='Get list Answer Question'
    )
    def get(self, request):
        answer_question = AnswerQuestion.objects.all()
        serializer = AnswerQuesionViewSerializer(answer_question, many=True)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Answer Question"],
        responses=status_response,
        operation_id='Create Answer Question',
        operation_description='Create Answer Question',
        request_body=swg.post_answerquestion
    )
    @transaction.atomic
    def post(self, request):
        serializer = AnswerQuesionViewSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class AnswerQuestionDetail(APIView):

    @swagger_auto_schema(
        tags=["Answer Question"],
        responses=status_response,
        operation_id='Get detail Answer Question',
        operation_description='Get detail Answer Question'
    )
    def get(self, request, pk):
        try:
            answer_question = AnswerQuestion.objects.get(pk=pk)
        except AnswerQuestion.DoesNotExist:
            return Response(convert_response("AnswerQuestion not found", 400))
        serializer = AnswerQuesionViewSerializer(answer_question)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Answer Question"],
        responses=status_response,
        operation_id='Update Answer Question',
        operation_description='Update Answer Question',
        request_body=swg.post_answerquestion
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            answer_question = AnswerQuestion.objects.get(pk=pk)
        except AnswerQuestion.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = AnswerQuesionViewSerializer(answer_question, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Answer Question"],
        responses=status_response,
        operation_id='Delete Answer Question',
        operation_description='Delete Answer Question'
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            answer_question = AnswerQuestion.objects.get(pk=pk)
        except AnswerQuestion.DoesNotExist:
            return Response(convert_response("AnswerQuestion not found", 400))
        answer_question.delete()
        return Response(convert_response("Success", 200))