from django.urls import path
from answerquestion.views import AnswerQuestionView, AnswerQuestionDetail

urlpatterns = [
    path("api/answerquestion/", AnswerQuestionView.as_view()),
    path("api/answerquestion/<int:pk>/", AnswerQuestionDetail.as_view()),
]