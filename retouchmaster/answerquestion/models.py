from django.db import models
from django.utils import timezone
from service.models import Service
from account.models import Account

class AnswerQuestion(models.Model):
    answer     = models.TextField(blank=True, null=True, default=None)
    question   = models.TextField(blank=True, null=True, default=None)

    user_created  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_answerquestion")
    user_updated  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_answerquestion")
    created_at	  = models.DateTimeField(default=timezone.now)
    updated_at	  = models.DateTimeField(default=timezone.now)