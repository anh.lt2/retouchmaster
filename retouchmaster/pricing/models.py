from django.db import models
from django.utils import timezone
from account.models import Account
from service.models import Service


class Pricing(models.Model):
    price           = models.FloatField(blank=True, default=0)
    service         = models.ForeignKey(Service, on_delete=models.CASCADE, null=True, blank=True)

    user_created    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_pricing")
    user_updated    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_pricing")
    created_at	    = models.DateTimeField(default=timezone.now)
    updated_at	    = models.DateTimeField(default=timezone.now)
