from django.urls import path
from pricing.views import PricingView, PricingDetail

urlpatterns = [
    path("api/pricing/", PricingView.as_view()),
    path("api/pricing/<int:pk>/", PricingDetail.as_view()),
]