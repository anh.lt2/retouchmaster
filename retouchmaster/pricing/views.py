from rest_framework.views import APIView, Response
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from pricing.models import Pricing
from pricing.serializers import PricingSerializer


class PricingView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Pricing"],
        responses=status_response,
        operation_id='Get list Pricing',
        operation_description='Get list Trial filter = ["service__title", "content", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["service__title", "content", "search"]
        SEARCH_FIELDS = ["service__title", "content"]
        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        pricing = Pricing.objects.filter(**query_params).filter(search_query)
        count = pricing.count()
        pricing = Paginate(pricing, request.GET)
        serializer = PricingSerializer(pricing, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))
    
    @swagger_auto_schema(
        tags=["Pricing"],
        responses=status_response,
        operation_id='Create Pricing',
        operation_description='Create Pricing',
        request_body=swg.post_pricing
    )
    @transaction.atomic
    def post(self, request):
        serializer = PricingSerializer(data=request.data, context={"request": request})
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400, serializer.errors))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class PricingDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Pricing"],
        responses=status_response,
        operation_id='Get Detail Pricing',
        operation_description='Get Detail Pricing'
    )
    def get(self, request, pk):
        try:
            pricing = Pricing.objects.get(pk=pk)
        except Pricing.DoesNotExist:
            return Response(convert_response("Pricing not found", 400))
        serializer = PricingSerializer(pricing)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Pricing"],
        responses=status_response,
        operation_id='Update Pricing',
        operation_description='Update Pricing',
        request_body=swg.post_pricing
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            pricing = Pricing.objects.get(pk=pk)
        except Pricing.DoesNotExist:
            return Response(convert_response("Pricing not found", 400))
        serializer = PricingSerializer(pricing, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Pricing"],
        responses=status_response,
        operation_id='Delete Pricing',
        operation_description='Delete Pricing',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            pricing = Pricing.objects.get(pk=pk)
        except Pricing.DoesNotExist:
            return Response(convert_response("Pricing not found", 400))
        pricing.delete()
        return Response(convert_response("Success", 200))