import pyrebase
import requests
from mykiotbe.settings import firebaseConfig, KEY_FCM

# Try connect to firebase
try:
    firebase = pyrebase.initialize_app(firebaseConfig)
    auth = firebase.auth()
    database = firebase.database()
except:
    pass

# FCM clound messaging
def send_notification_topic(title, content, code, topic, data):
    payload = { 
        "notification" : {
            "click_action" : "FLUTTER_NOTIFICATION_CLICK", 
            "body" : content, 
            "title" : title, 
            "code": code,
            "icon" : None
        }, 
        "data": data,
        "to" : "/topics/{}".format(topic)
    }
    header = {
        "Content-Type": "application/json",
        "Authorization": KEY_FCM
    }
    try:
        response = requests.post('https://fcm.googleapis.com/fcm/send', json=payload, headers=header).json()
    except: 
        pass