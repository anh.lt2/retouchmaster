from rest_framework.views import APIView, Response
from quote.models import Quote
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from quote.serializers import QuoteViewSerializer, CreateMultipleMediaSerializer

class QuoteView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Quote"],
        responses=status_response,
        operation_id='Get list Quote',
        operation_description='Get list Quote filter = ["customer_name", "email", "note", "status", "service__title", "require_auth", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["customer_name", "email", "status", "note", "service__title", "require_auth", "search"]
        SEARCH_FIELDS = ["customer_name", "email", "note", "service__title"]
        BOOL_ACCEPT = ["status"]
        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key in BOOL_ACCEPT:
                if query_params[key] == "true":
                    query_params[key] = True
                elif query_params[key] == "false":
                    query_params[key] = False
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        quote = Quote.objects.filter(**query_params).filter(search_query).order_by("-id")
        count = quote.count()
        quote = Paginate(quote, request.GET)
        serializer = QuoteViewSerializer(quote, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))
    
    @swagger_auto_schema(
        tags=["Quote"],
        responses=status_response,
        operation_id='Create Quote',
        operation_description='Create Quote',
        request_body=swg.create_quote
    )
    @transaction.atomic
    def post(self, request):
        media_quote = request.data.get("media_quote", None)
        serializer = QuoteViewSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400, serializer.errors))
        quote_instance = serializer.save()
        if media_quote:
            serializer_media = CreateMultipleMediaSerializer(data=request.data)
            if not serializer_media.is_valid():
                quote_instance.delete()
                return Response(convert_response("Error", 400, serializer_media.errors))
            media_instance = serializer_media.save()
            quote_instance.image.set(media_instance)
        return Response(convert_response("Success", 200, serializer.data))
    
class QuoteDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Quote"],
        responses=status_response,
        operation_id='Get detail Quote',
        operation_description='Get detail Quote',
    )
    def get(self, request, pk):
        try:
            quote = Quote.objects.get(pk=pk)
        except Quote.DoesNotExist:
            return Response(convert_response("Quote not found", 400))
        serializer = QuoteViewSerializer(quote)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Quote"],
        responses=status_response,
        operation_id='Update Quote',
        operation_description='Update Quote',
        request_body=swg.create_quote
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            quote = Quote.objects.get(pk=pk)
        except Quote.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = QuoteViewSerializer(quote, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Quote"],
        responses=status_response,
        operation_id='Delete Quote',
        operation_description='Delete Quote',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            quote = Quote.objects.get(pk=pk)
        except Quote.DoesNotExist:
            return Response(convert_response("Quote not found", 400))
        quote.delete()
        return Response(convert_response("Success", 200))