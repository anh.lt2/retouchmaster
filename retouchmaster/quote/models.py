from django.db import models
from django.utils import timezone
from service.models import Service
from account.models import Account

class MediaQuote(models.Model):
    image       = models.FileField(upload_to='quote/image/')
    alt         = models.CharField(max_length=255, null=True, blank=True, default=None)
    created_at  = models.DateTimeField(default=timezone.now)
    updated_at  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.alt

class Quote(models.Model):
    customer_name = models.CharField(max_length=255, blank=True, null=True, default=None)
    email         = models.CharField(max_length=255, blank=True, null=True, default=None)
    note          = models.TextField(null=True, blank=True, default=None)
    url           = models.TextField(blank=True, null=True, default=None)
    status        = models.BooleanField(blank=True, null=True, default=False)
    image         = models.ManyToManyField(MediaQuote, blank=True)
    service       = models.ForeignKey(Service, on_delete=models.SET_NULL, null=True, blank=True, default=None )

    user_created    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_quote")
    user_updated    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_quote")
    created_at	    = models.DateTimeField(default=timezone.now)
    updated_at	    = models.DateTimeField(default=timezone.now)