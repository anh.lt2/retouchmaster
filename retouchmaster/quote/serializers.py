from rest_framework import serializers
from general.general import *
from quote.models import Quote, MediaQuote
from service.serializers import ServiceSelectSerializer

class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaQuote
        fields = ["id", "image", "alt", "created_at", "updated_at"]
        
class CreateMultipleMediaSerializer(serializers.Serializer):
    media_quote = serializers.ListField(child=serializers.FileField())

    def create(self, validate_data):
        try:
            media = MediaQuote._default_manager.bulk_create(
                MediaQuote(image=image, alt=image.name) for image in validate_data["media_quote"]
            )
            return media
        except:
            raise serializers.ValidationError({"Error" : "Invalids params media_quote"})

class QuoteViewSerializer(serializers.ModelSerializer):
    service_data = ServiceSelectSerializer(source="service", read_only=True)
    image_data = MediaSerializer(source="image", read_only=True, many=True)
    class Meta:
        model = Quote
        fields = ["id", "customer_name", "email", "note", "image", "image_data", "url", "status", "service", "service_data", "user_created", "user_updated", "created_at", "updated_at"]