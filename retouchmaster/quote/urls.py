from django.urls import path
from quote.views import QuoteView, QuoteDetail

urlpatterns = [
    path("api/quote/", QuoteView.as_view()),
    path("api/quote/<int:pk>/", QuoteDetail.as_view()),
]