from django.db import models
from django.utils import timezone
from service.models import Service
from account.models import Account

class Product(models.Model):
    titlte        = models.CharField(max_length=255, blank=True, null=True, default=None)
    image_before  = models.FileField(upload_to='product/image_before/', null=True, blank=True)
    image_after   = models.FileField(upload_to='product/image_after/', null=True, blank=True)
    image_example = models.FileField(upload_to='product/image_example/', null=True, blank=True)
    video_url     = models.TextField(blank=True, null=True, default=None)
    content       = models.TextField(blank=True, null=True, default=None)
    sort          = models.IntegerField(null=True, blank=True, default=0)
    status_active = models.BooleanField(default=False)
    status_home   = models.BooleanField(default=False)
    service       = models.ForeignKey(Service, on_delete=models.SET_NULL, null=True, blank=True, default=None )

    user_created  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_product")
    user_updated  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_product")
    created_at	  = models.DateTimeField(default=timezone.now)
    updated_at	  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.titlte