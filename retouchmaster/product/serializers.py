from rest_framework import serializers
from general.general import *
from product.models import Product

class ProductViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ["id", "image_before", "image_after", "image_example", "video_url", "content", "sort", "status_active", "status_home", "service", "user_created", "user_updated", "created_at", "updated_at"] 