from rest_framework.views import APIView, Response
from product.models import Product
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from product.serializers import ProductViewSerializer

class ProductView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Product"],
        responses=status_response,
        operation_id='Get list Product',
        operation_description='Get list Product'
    )
    def get(self, request):
        product = Product.objects.all()
        serializer = ProductViewSerializer(product, many=True)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Product"],
        responses=status_response,
        operation_id='Create Product',
        operation_description='Create Product',
        request_body=swg.create_product
    )
    @transaction.atomic
    def post(self, request):
        serializer = ProductViewSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class ProductDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Product"],
        responses=status_response,
        operation_id='Get detail Product',
        operation_description='Get detail Product',
    )
    def get(self, request, pk):
        try:
            product = Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return Response(convert_response("Product not found", 400))
        serializer = ProductViewSerializer(product)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Product"],
        responses=status_response,
        operation_id='Update Product',
        operation_description='Update Product',
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            product = Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = ProductViewSerializer(product, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Product"],
        responses=status_response,
        operation_id='Delete Product',
        operation_description='Delete Product',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try: 
            product = Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return Response(convert_response("Product not found", 400))
        product.delete()
        return Response(convert_response("Success", 200))