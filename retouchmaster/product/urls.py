from django.urls import path
from product.views import ProductView, ProductDetail

urlpatterns = [
    path("api/product/", ProductView.as_view()),
    path("api/product/<int:pk>/", ProductDetail.as_view()),

]