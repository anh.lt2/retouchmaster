from django.urls import path
from processes.views import ProcessesView, ProcessesDetail

urlpatterns = [
    path("api/processes/", ProcessesView.as_view()),
    path("api/processes/<int:pk>/", ProcessesDetail.as_view()),
]