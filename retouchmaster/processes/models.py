from django.db import models
from django.utils import timezone
from account.models import Account

# Create your models here.

class Processes(models.Model):
    title           = models.CharField(max_length=255, blank=True, null=True, default=None)
    content         = models.TextField(blank=True, null=True, default=None)
    status          = models.BooleanField(default=False)
    image           = models.FileField(upload_to='processes/image/', blank=True, null=True)

    user_created    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_processes")
    user_updated    = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_processes")
    created_at	    = models.DateTimeField(default=timezone.now)
    updated_at	    = models.DateTimeField(default=timezone.now)