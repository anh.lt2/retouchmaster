from rest_framework import serializers
from general.general import *
from processes.models import Processes

class ProcessesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Processes
        fields = ["id", "title", "content", "status", "image", "user_created", "user_updated", "created_at", "updated_at"]

    def create(self, validated_data):
        user = self.context["request"].user
        validated_data["user_created"] = user
        validated_data["user_updated"] = user
        return super().create(validated_data)