from rest_framework.views import APIView, Response
from django.db.models import Q
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from processes.models import Processes
from processes.serializers import ProcessesSerializer

class ProcessesView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Processes"],
        responses=status_response,
        operation_id='Get list Processes',
        operation_description='Get list Processes filter =["title", "content", "status", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["title", "content", "status", "search"]
        SEARCH_FIELDS = ["title", "content"]
        BOOL_ACCEPT = ["status"]

        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key in BOOL_ACCEPT:
                if query_params[key] == "true":
                    query_params[key] = True
                elif query_params[key] == "false":
                    query_params[key] = False
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        processes = Processes.objects.filter(search_query, **query_params)
        count = processes.count() 
        processes = Paginate(processes, request.GET)
        serializer = ProcessesSerializer(processes, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))

    @swagger_auto_schema(
        tags=["Processes"],
        responses=status_response,
        operation_id='Post Processes',
        operation_description='Post Processes',
        request_body=swg.post_processes
    )
    @transaction.atomic
    def post(self, request):
        serializer = ProcessesSerializer(data=request.data, context={"request": request})
        if not serializer.is_valid():
            return Response(convert_response("Invalid params processes", 200, serializer.errors)) 
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class ProcessesDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Processes"],
        responses=status_response,
        operation_id='Get detail Processes',
        operation_description='Get detail Processes',
    )
    def get(self, request, pk):
        try:
            processes = Processes.objects.get(pk=pk)
        except Processes.DoesNotExist:
            return Response(convert_response("Processes not found", 400))
        serializer = ProcessesSerializer(processes)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Processes"],
        responses=status_response,
        operation_id='Update Processes',
        operation_description='Update Processes',
        request_body=swg.post_processes
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            processes = Processes.objects.get(pk=pk)
        except Processes.DoesNotExist:
            return Response(convert_response("Processes not found", 400))
        serializer = ProcessesSerializer(processes, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400, serializer.errors))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Processes"],
        responses=status_response,
        operation_id='Delete Processes',
        operation_description='Delete Processes',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            processes = Processes.objects.get(pk=pk)
        except Processes.DoesNotExist:
            return Response(convert_response("Processes not found", 400))
        processes.delete()
        return Response(convert_response("Success", 200))