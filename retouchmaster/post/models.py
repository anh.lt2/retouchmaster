from django.db import models
from django.utils import timezone
from service.models import Service
from account.models import Account


class MediaPost(models.Model):
    image       = models.FileField(upload_to='mediapost/image/')
    alt         = models.CharField(max_length=255, null=True, blank=True, default=None)
    created_at  = models.DateTimeField(default=timezone.now)
    updated_at  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.alt

class Category(models.Model):
    name          = models.CharField(max_length=255, blank=True, null=True, default=None)
    pathurl       = models.TextField(blank=True, null=True, default=None)

    user_created  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_category")
    user_updated  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_category")
    created_at	  = models.DateTimeField(default=timezone.now)
    updated_at	  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name

class Post(models.Model):
    title         = models.CharField(max_length=255, blank=True, null=True, default=None)
    tag           = models.CharField(max_length=255, blank=True, null=True, default=None)
    url           = models.TextField(blank=True, null=True, default=None, unique=True)
    image         = models.FileField(upload_to='post/image/', null=True, blank=True)
    media         = models.ManyToManyField(MediaPost, blank=True)
    summary       = models.TextField(blank=True, null=True, default=None)
    description   = models.TextField(blank=True, null=True, default=None)
    content       = models.TextField(blank=True, null=True, default=None)
    content_bonus = models.TextField(blank=True, null=True, default=None)
    is_highlight  = models.BooleanField(default=False)
    status        = models.BooleanField(default=False)
    status_new    = models.BooleanField(default=False)
    top_blog      = models.BooleanField(default=False)
    lastest_blog  = models.BooleanField(default=False)
    subscribers   = models.CharField(max_length=255, blank=True, null=True, default=None)
    category      = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True, default=None)

    user_created  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_post")
    user_updated  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_post")
    created_at	  = models.DateTimeField(default=timezone.now)
    updated_at	  = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.title