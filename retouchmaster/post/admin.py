from django.contrib import admin
from post.models import Post, Category, MediaPost

# Register your models here.
admin.site.register(Post)
admin.site.register(Category)
admin.site.register(MediaPost)