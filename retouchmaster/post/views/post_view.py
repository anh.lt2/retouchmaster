from rest_framework.views import APIView, Response
from post.models import Post
from django.db.models import Q
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from post.serializers import PostViewSerializer, CreateMultipleMediaSerializer, PostDetailSerializer

class PostView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Post"],
        responses=status_response,
        operation_id='Get list Post',
        operation_description='Get list Post filter = ["title", "content", "description", "summary", "category", "status_new", "status", "top_blog", "lastest_blog", "created_at__lte", "created_at__gte", "require_auth", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["title", "content", "description", "summary", "category", "status_new", "subscribers", "status", "is_highlight", "top_blog", "lastest_blog", "created_at__lte", "created_at__gte", "random", "require_auth", "search"]
        SEARCH_FIELDS = ["title", "content", "description", "summary", "subscribers", "category__name"]
        BOOL_ACCEPT = ["status_new", "status", "is_highlight", "top_blog", "lastest_blog", "random"]

        # Đảm bảo rằng bạn đã gán giá trị cho query_params trước khi sử dụng nó
        query_params = request.query_params.dict().copy()
        # Tiếp tục xử lý tham số "random" và các tham số khác
        is_random = query_params.pop("random", None) == "true"       
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key in BOOL_ACCEPT:
                if query_params[key] == "true":
                    query_params[key] = True
                elif query_params[key] == "false":
                    query_params[key] = False
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        post = Post.objects.filter(**query_params).filter(search_query).order_by("-id")
        count = post.count() 
        if is_random:
            post = list(post)  # Chuyển queryset thành danh sách
            random.shuffle(post)  # Xáo trộn danh sách
        post = Paginate(post, request.GET)
        serializer = PostViewSerializer(post, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))
    
    @swagger_auto_schema(
        tags=["Post"],
        responses=status_response,
        operation_id='Create Post',
        operation_description='Create Post',
        request_body=swg.create_post
    )
    @transaction.atomic
    def post(self, request):
        media_post = request.data.get("media_post", None)

        serializer = PostDetailSerializer(data=request.data, context={"request": request})
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400, serializer.errors))
        post_instance = serializer.save()

        if media_post:
            serializer_media = CreateMultipleMediaSerializer(data=request.data)
            if not serializer_media.is_valid():
                post_instance.delete()
                return Response(convert_response("Error", 400, serializer_media.errors))
            media_instance = serializer_media.save()
            post_instance.media.set(media_instance)
        return Response(convert_response("Success", 200, serializer.data))
    
class PostDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Post"],
        responses=status_response,
        operation_id='Get Detail Post',
        operation_description='Get Detail Post',
    )
    def get(self, request, slug):
        try:
            post = Post.objects.get(url=slug)
        except Post.DoesNotExist:
            return Response(convert_response("Post not found", 400))
        serializer = PostDetailSerializer(post)
        return Response(convert_response("Success", 200, serializer.data))

class UpdatePost(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Post"],
        responses=status_response,
        operation_id='Update Post',
        operation_description='Update Post',
        request_body=swg.create_post
    )
    @transaction.atomic
    def put(self, request, pk):
        media_data = request.data.get("media_post", None)

        try:
            post = Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            return Response(convert_response("Post not found", 400))
        serializer = PostDetailSerializer(post, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        post_instance = serializer.save() 

        if media_data:
            serializer_media = CreateMultipleMediaSerializer(data=request.data)
            if not serializer_media.is_valid():
                return Response(convert_response("Error", 400, serializer_media.errors))
            media_instance = serializer_media.save()
            post_instance.media.set(media_instance)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Post"],
        responses=status_response,
        operation_id='Delete Post',
        operation_description='Delete Post'
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            post = Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            return Response(convert_response("Post not found", 400))
        post.delete()
        return Response(convert_response("Success", 200))
    
class PostDetailWithID(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Post"],
        responses=status_response,
        operation_id='Get Detail With ID Post',
        operation_description='Get Detail With ID Post',
    )
    def get(self, request, pk):
        try:
            post = Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            return Response(convert_response("Post not found", 400))
        serializer = PostDetailSerializer(post)
        return Response(convert_response("Success", 200, serializer.data))