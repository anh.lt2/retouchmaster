from rest_framework.views import APIView, Response
from post.models import Category
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from django.db.models import Q
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from post.serializers import CategoryViewSerializer

class CategoryView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Category"],
        responses=status_response,
        operation_id='Get list Category',
        operation_description='Get list Category'
    )
    def get(self, request):
        QUERY_ACCEPT = ["name", "pathurl", "require_auth", "search"]
        SEARCH_FIELDS = ["name", "pathurl"]

        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        category = Category.objects.filter(**query_params).filter(search_query)
        serializer = CategoryViewSerializer(category, many=True)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Category"],
        responses=status_response,
        operation_id='Create Category',
        operation_description='Create Category',
        request_body=swg.create_category
    )
    @transaction.atomic
    def post(self, request):
        serializer = CategoryViewSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class CategoryDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Category"],
        responses=status_response,
        operation_id='Get detail Category',
        operation_description='Get detail Category'
    )
    def get(self, request, pk):
        try:
            category = Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            return Response(convert_response("Category not found", 400))
        serializer = CategoryViewSerializer(category)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Category"],
        responses=status_response,
        operation_id='Update Category',
        operation_description='Update Category',
        request_body=swg.create_category
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            category = Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            return Response(convert_response("Service not found", 400))
        serializer = CategoryViewSerializer(category, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Category"],
        responses=status_response,
        operation_id='Delete Category',
        operation_description='Delete Category'
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            category = Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            return Response(convert_response("Category not found", 400))
        category.delete()
        return Response(convert_response("Success", 200))