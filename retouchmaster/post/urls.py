from django.urls import path
from post.views.category_views import CategoryView, CategoryDetail
from post.views.post_view import PostView, PostDetail, UpdatePost, PostDetailWithID

urlpatterns = [
    path("api/category/", CategoryView.as_view()),
    path("api/category/<int:pk>/", CategoryDetail.as_view()),
    path("api/post/", PostView.as_view()),
    path("api/post/<int:pk>/", UpdatePost.as_view()),
    path("api/getdetailpost/<int:pk>/", PostDetailWithID.as_view()),
    path("api/post/<str:slug>/", PostDetail.as_view()),
]