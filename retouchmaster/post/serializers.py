from rest_framework import serializers
from general.general import *
from post.models import Post, Category, MediaPost

class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaPost
        fields = ["id", "image", "alt", "created_at", "updated_at"]

class CreateMultipleMediaSerializer(serializers.Serializer):
    media_post = serializers.ListField(child=serializers.FileField())

    def create(self, validate_data):
        try:
            media = MediaPost._default_manager.bulk_create(
                MediaPost(image=image, alt=image.name) for image in validate_data["media_post"]
            )
            return media
        except:
            raise serializers.ValidationError({"Error" : "Invalids params media_service"})
        
class CategoryViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ["id", "name", "pathurl", "user_created", "user_updated", "created_at", "updated_at"]

class PostDetailSerializer(serializers.ModelSerializer):
    media_data = MediaSerializer(source="media", read_only=True, many=True)
    category_data = CategoryViewSerializer(source="category", read_only=True)

    class Meta:
        model = Post
        fields = ["id", "title", "tag", "url", "image", "media", "media_data", "content_bonus", "category", "category_data", "summary", "description", "content", "is_highlight", "status", "status_new", "top_blog", "lastest_blog", "subscribers", "user_created", "user_updated", "created_at", "updated_at"]

    def create(self, validated_data):
        account = self.context["request"].user
        validated_data["user_created"] = account
        validated_data["user_updated"] = account
        return super().create(validated_data)
    
class PostViewSerializer(serializers.ModelSerializer):
    category_data = CategoryViewSerializer(source="category", read_only=True)
    class Meta:
        model = Post
        fields = ["id", "title", "url", "image", "category", "category_data", "is_highlight", "status", "status_new", "top_blog", "lastest_blog", "subscribers", "created_at", "updated_at"]