from django.urls import path
from trial.views import TrialView, TrialDetail, PostTrial

urlpatterns = [
    path("api/trial/", TrialView.as_view()),
    path("api/trial/<int:pk>/", TrialDetail.as_view()),
    path("api/postmedia/", PostTrial.as_view()),
]