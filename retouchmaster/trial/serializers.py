from rest_framework import serializers
from general.general import *
from trial.models import Trial, MediaTrial
from service.serializers import ServiceSelectSerializer


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTrial
        fields = ["id", "image", "alt", "created_at", "updated_at"]
        
class CreateMultipleMediaSerializer(serializers.Serializer):
    media_trial = serializers.ListField(child=serializers.FileField())

    def create(self, validate_data):
        try:
            media = MediaTrial._default_manager.bulk_create(
                MediaTrial(image=image, alt=image.name) for image in validate_data["media_trial"]
            )
            return media
        except:
            raise serializers.ValidationError({"Error" : "Invalids params media_trial"})

class CreatemultilSerializer(serializers.Serializer):
    file = serializers.ListField(child=serializers.FileField(), write_only=True, required=True)

    def create(self, validate_data):
        try:
            media_list = []
            for item in validate_data["file"]:
                media = MediaTrial.objects.create(image=item, alt=item.name)
                media_list.append(media)      
            return media_list
        except:
            raise serializers.ValidationError({"Error" : "Invalid params in file"})


class TrialViewSerializer(serializers.ModelSerializer):
    service_data = ServiceSelectSerializer(source="service", read_only=True)
    image_data = MediaSerializer(source="image", read_only=True, many=True)
    class Meta:
        model = Trial
        fields = ["id", "customer_name", "email", "note", "image", "image_data", "url", "status", "service", "service_data", "user_created", "user_updated", "created_at", "updated_at"]