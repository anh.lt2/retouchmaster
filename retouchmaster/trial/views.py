from rest_framework.views import APIView, Response
from trial.models import Trial
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from trial.serializers import TrialViewSerializer, CreateMultipleMediaSerializer, CreatemultilSerializer

class TrialView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Trial"],
        responses=status_response,
        operation_id='Get list Trial',
        operation_description='Get list Trial filter = ["customer_name", "status", "email", "note", "service__title", "require_auth", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["customer_name", "email", "status", "note", "service__title", "require_auth", "search"]
        SEARCH_FIELDS = ["customer_name", "email", "note", "service__title"]
        BOOL_ACCEPT = ["status"]
        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key in BOOL_ACCEPT:
                if query_params[key] == "true":
                    query_params[key] = True
                elif query_params[key] == "false":
                    query_params[key] = False
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        trial = Trial.objects.filter(**query_params).filter(search_query).order_by("-id")
        count = trial.count()
        trial = Paginate(trial, request.GET)
        serializer = TrialViewSerializer(trial, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))
    
    @swagger_auto_schema(
        tags=["Trial"],
        responses=status_response,
        operation_id='Create Trial',
        operation_description='Create Trial',
        request_body=swg.post_trial
    )
    @transaction.atomic
    def post(self, request):
        media_trial = request.data.get("media_trial", None)
        serializer = TrialViewSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400, serializer.errors))
        trial_instance = serializer.save()
        if media_trial:
            serializer_media = CreateMultipleMediaSerializer(data=request.data)
            if not serializer_media.is_valid():
                trial_instance.delete()
                return Response(convert_response("Error", 400, serializer_media.errors))
            media_instance = serializer_media.save()
            trial_instance.image.set(media_instance)
        return Response(convert_response("Success", 200, serializer.data))
    
class TrialDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Trial"],
        responses=status_response,
        operation_id='Get Detail Trial',
        operation_description='Get Detail Trial'
    )
    def get(self, request, pk):
        try:
            trial = Trial.objects.get(pk=pk)
        except Trial.DoesNotExist:
            return Response(convert_response("trial not found", 400))
        serializer = TrialViewSerializer(trial)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Trial"],
        responses=status_response,
        operation_id='Update Trial',
        operation_description='Update Trial',
        request_body=swg.post_trial
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            trial = Trial.objects.get(pk=pk)
        except Trial.DoesNotExist:
            return Response(convert_response("trial not found", 400))
        serializer = TrialViewSerializer(trial, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Trial"],
        responses=status_response,
        operation_id='Delete Trial',
        operation_description='Delete Trial',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            trial = Trial.objects.get(pk=pk)
        except Trial.DoesNotExist:
            return Response(convert_response("trial not found", 400))
        trial.delete()
        return Response(convert_response("Success", 200))
    
class PostTrial(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Trial"],
        responses=status_response,
        operation_id='Create Trial Media',
        operation_description='Create Trial Media',
        request_body=swg.post_media
    )
    @transaction.atomic
    def post(self, request):
        file = request.data.get("file", None)
        serializer_media = CreatemultilSerializer(data=request.data)
        if not serializer_media.is_valid():
            return Response(convert_response("Error", 400, serializer_media.errors))
        media_instance = serializer_media.save()
        media_data = [{"image": media.image.url, "alt": media.alt} for media in media_instance]
        return Response(convert_response("Success", 200, media_data))