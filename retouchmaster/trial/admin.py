from django.contrib import admin
from trial.models import Trial, MediaTrial

# Register your models here.
admin.site.register(Trial)
admin.site.register(MediaTrial)