from django.db import models
from django.utils import timezone
from service.models import Service
from account.models import Account

class Feedback(models.Model):
    customer_name = models.CharField(max_length=255, blank=True, null=True, default=None)
    position      = models.CharField(max_length=255, blank=True, null=True, default=None)
    rate          = models.IntegerField(null=True, blank=True, default=0)
    avatar        = models.ImageField(upload_to='feedback/avatar/', null=True, blank=True)
    evaluate      = models.TextField(blank=True, null=True, default=None)
    status        = models.BooleanField(default=False)

    user_created  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_created_feedback")
    user_updated  = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True, default=None, related_name="user_updated_feedback")
    created_at	  = models.DateTimeField(default=timezone.now)
    updated_at	  = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.customer_name