from django.urls import path
from feedback.views import FeedbackDetail, FeedbackView

urlpatterns = [
    path("api/feedback/", FeedbackView.as_view()),
    path("api/feedback/<int:pk>/", FeedbackDetail.as_view()),
]