from rest_framework import serializers
from general.general import *
from feedback.models import Feedback

class FeedbackViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feedback
        fields = ["id", "customer_name", "position", "rate", "status", "avatar", "evaluate", "user_created", "user_updated", "created_at", "updated_at"]

    def create(self, validated_data):
        user = self.context["request"].user
        validated_data["user_created"] = user
        validated_data["user_updated"] = user
        return super().create(validated_data)