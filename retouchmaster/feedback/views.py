from rest_framework.views import APIView, Response
from rest_framework.permissions import IsAuthenticated
from django.db import transaction
from django.db.models import Q
from drf_yasg.utils import swagger_auto_schema
import generic.swagger_example as swg
from general.general import *
from feedback.models import Feedback
from feedback.serializers import FeedbackViewSerializer

class FeedbackView(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Feedback"],
        responses=status_response,
        operation_id='Get list Feedback',
        operation_description='Get list Feedback filter=["customer_name", "status", "position", "evaluate", "service__title", "require_auth", "search"]'
    )
    def get(self, request):
        QUERY_ACCEPT = ["customer_name", "position", "evaluate", "status", "service__title", "require_auth", "search"]
        SEARCH_FIELDS = ["customer_name", "position", "evaluate"]
        BOOL_ACCEPT = ["status"]
        query_params = request.query_params.dict().copy()
        search_query = Q()
        for key in query_params.copy().keys():
            if key not in QUERY_ACCEPT or key in ["page", "limit"]:
                del query_params[key]
            if key in BOOL_ACCEPT:
                if query_params[key] == "true":
                    query_params[key] = True
                elif query_params[key] == "false":
                    query_params[key] = False
            if key == "search":
                for field in SEARCH_FIELDS:
                    search_query |= Q(**{f"{field}__icontains": query_params["search"]})
                del query_params["search"]
        feedback = Feedback.objects.filter(search_query, **query_params)
        count = feedback.count()
        feedback = Paginate(feedback, request.GET)
        serializer = FeedbackViewSerializer(feedback, many=True)
        return Response(convert_response("Success", 200, serializer.data, count=count))
    
    @swagger_auto_schema(
        tags=["Feedback"],
        responses=status_response,
        operation_id='Create Feedback',
        operation_description='Create Feedback',
        request_body=swg.post_feedback
    )
    @transaction.atomic
    def post(self, request):
        serializer = FeedbackViewSerializer(data=request.data, context={"request": request})
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save()
        return Response(convert_response("Success", 200, serializer.data))
    
class FeedbackDetail(APIView):
    permission_classes = [ConditionalIsAuthenticatedPermission]

    @swagger_auto_schema(
        tags=["Feedback"],
        responses=status_response,
        operation_id='Get Detail Feedback',
        operation_description='Get Detail Feedback'
    )
    def get(self, request, pk):
        try:
            feedback = Feedback.objects.get(pk=pk)
        except Feedback.DoesNotExist:
            return Response(convert_response("Feedback not found", 400))
        serializer = FeedbackViewSerializer(feedback)
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Feedback"],
        responses=status_response,
        operation_id='Update Feedback',
        operation_description='Update Feedback',
        request_body=swg.post_feedback
    )
    @transaction.atomic
    def put(self, request, pk):
        try:
            feedback = Feedback.objects.get(pk=pk)
        except Feedback.DoesNotExist:
            return Response(convert_response("Feedback not found", 400))
        serializer = FeedbackViewSerializer(feedback, request.data, partial=True)
        if not serializer.is_valid():
            return Response(convert_response("Invalid params", 400))
        serializer.save() 
        return Response(convert_response("Success", 200, serializer.data))
    
    @swagger_auto_schema(
        tags=["Feedback"],
        responses=status_response,
        operation_id='Delete Feedback',
        operation_description='Delete Feedback',
    )
    @transaction.atomic
    def delete(self, request, pk):
        try:
            feedback = Feedback.objects.get(pk=pk)
        except Feedback.DoesNotExist:
            return Response(convert_response("Feedback not found", 400))
        feedback.delete()
        return Response(convert_response("Success", 200))